%recognize the card and get the result from neural network output
count=length(poker_outputs(1,:));
i=1;
k=1;
while i<count
    r_vector = poker_outputs(:,i);
    s_vector = poker_outputs(:,i+1);
    rank = getSuitAndRank(r_vector);
    suit = getSuitAndRank(s_vector);
    i=i+2;
    strcat("card_",num2str(k)," is ",suit," ",rank)
    k=k+1;
end 