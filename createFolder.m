%create a folder, if a folder is already exist, 
%delete it and create it again
function createFolder(path)
    if ~exist(path,'dir')
        mkdir(path);
    else
        rmdir(path,'s');
        mkdir(path);
    end
end