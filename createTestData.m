clear;
clc;

%processing input images
readInputDirPath = 'c:\project\inputImage\';
inputFileLists = dir([readInputDirPath,'*.jpg']);
global card_num;
input_counts = length(inputFileLists);
card_num=1;
for i=1:input_counts
    image_name = inputFileLists(i).name;
    savedFolderPath = strcat(readInputDirPath,'card_',num2str(card_num));
    saveImagePath = strcat(savedFolderPath,'\');
    readImagePath = strcat(readInputDirPath,image_name);
    
    %save image folder
    createFolder(savedFolderPath);
    %read image file path
    processImage(readImagePath,saveImagePath);
    card_num=card_num+1;  
end 

testPath = 'c:\project\inputImage\';
targetFolderList = dir([testPath,'card_*']);
if exist('test_set.csv','file')==2
    delete('test_set.csv')
end
%create the test set data
for i=1:length(targetFolderList)
    item=targetFolderList(i);
    targetFolder = strcat(item.folder,'\',item.name);
    rankImagePath = strcat(targetFolder,'\','rank.bmp');
    suitImagePath = strcat(targetFolder,'\','suit.bmp');
    rankImage = imread(rankImagePath);
    suitImage = imread(suitImagePath);
    resizeImage(rankImagePath,rankImagePath);
    resizeImageWithRotateIfNeeded(suitImagePath,suitImagePath);
    
    n_rankImage = imread(rankImagePath);
    n_rankImage_T=transpose(n_rankImage);
    imgVector_rank = reshape(n_rankImage_T,[1,2500]);
    dlmwrite('test_set.csv',imgVector_rank,'delimiter',',','-append');
    n_suitImage = imread(suitImagePath);
    n_suitImage_T=transpose(n_suitImage);
    imgVector_suit = reshape(n_suitImage_T,[1,2500]);
    dlmwrite('test_set.csv',imgVector_suit,'delimiter',',','-append');
end