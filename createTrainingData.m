function createTrainingData(filePath)
    if exist('training_set.csv','file')==2
        delete('training_set.csv')
    end
    if exist('target_set.csv','file')==2
        delete('target_set.csv')
    end
    readPath =getPath(filePath)
    for k = 2 : length(readPath)
        readSubFileList = dir([readPath{k},'*.bmp']);
        for j =1 : length(readSubFileList)
            temp =strsplit(readPath{k}, '\');
            suit = temp{end-1};
            tarVector = targetValue(suit);
            image_path = strcat(readPath{k},readSubFileList(j).name);
            image = imread(image_path);
            image_T = transpose(image);
            imageVector = reshape(image_T,[1,2500]);
            dlmwrite('training_set.csv',imageVector,'delimiter',',','-append');
            dlmwrite('target_set.csv',tarVector,'delimiter',',','-append');
        end
    end
end