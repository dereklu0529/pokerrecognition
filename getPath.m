%get file path
function [path] =getPath(file_path)
    gPath = genpath(file_path);
    length_gPath = size(gPath,2);
    path={};
    temp=[];
    index = 1;
    for i = 1 : length_gPath
        if gPath(i)~=';'
            temp = [temp gPath(i)];
        else
            temp = [temp '\'];
            path{index}=temp;
            index=index+1;
            temp=[];
        end
    end
    clear p length_p temp;
end