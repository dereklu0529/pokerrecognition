%get the result from neural network output
function result =getSuitAndRank(vector)
    [val,idx]=max(vector);
    r_vector = ["10","2","3","4","5","6","7","8"...
        ,"9","Ace","club","diamond","heart","Jack"...
        ,"King","Queen","spade"];
    result = r_vector(idx);
end