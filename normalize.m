%this script is used to create training set images
clear;
clc;
saveDirPath = 'c:\project\norm';
readDirPath = 'c:\project\save';
readPath =getPath(readDirPath);

%read images from save folders and resize images to specific folders
for k = 2 : length(readPath)
    readSubFileList = dir([readPath{k},'*.bmp']);
    currDirectory=readPath{k};
    temp =strsplit(currDirectory, '\');
    folderName = temp{end-1};
    saveFolder = char(strcat(saveDirPath,'\',folderName));
    createFolder(saveFolder);

    cnt=0;
    for j =1 : length(readSubFileList)
        read_file_path = strcat(readPath{k},readSubFileList(j).name)
        %saveFolder = strcat(savePath,'/',readSubFileList(j).name);
        save_file_path = strcat(saveFolder,'\',num2str(j),'.bmp')
        if folderName =='s' || folderName =='c'|| ...
                folderName=='h' ||folderName=='d'
            resizeImageWithRotateIfNeeded(read_file_path,save_file_path);
        else
            resizeImage(read_file_path,save_file_path);
        end
        cnt=cnt+1;
    end
end
