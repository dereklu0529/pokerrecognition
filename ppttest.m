clear
clc
readInputDirPath = 'c:\project\inputImage\';
inputFileLists = dir([readInputDirPath,'*.jpg']);
global card_num;
input_counts = length(inputFileLists);
card_num=1;
for i=1:input_counts
    image_name = inputFileLists(i).name;
    savedFolderPath = strcat(readInputDirPath,'card_',num2str(card_num));
    saveImagePath = strcat(savedFolderPath,'\');
    readImagePath = strcat(readInputDirPath,image_name);
    
    %save image folder
    createFolder(savedFolderPath);
    %read image file path
    processImage2(readImagePath,saveImagePath);
    card_num=card_num+1;  
end 

function processImage2(filePath,savePath)
    %read an image
    image = imread(filePath);
    %transform an image to gray scale
    gray = rgb2gray(image);
    %reduce size of an image
    resized = imresize(gray,0.3,'bicubic');
    %apply gaussian filter
    G=fspecial('gaussian',[5,5],1);
    gau_im=imfilter(resized,G,'same');
    %binarized an image
    bw = imbinarize(gau_im,0.6);
    imshow(bw)

    %filter noises and crop the image to normize into a fix size image
    measurements = regionprops(bw, 'Area','BoundingBox')
%     [idx,val]=max([measurements.Area])
%     measurements = regionprops('table',bw,'all')
%     for i=1:length(measurements)
%         if measurements(i).Area > 20000
%             thisBoundingBox=measurements(i).BoundingBox;  
%         end
% %         subImage = imcrop(bw,thisBoundingBox);
% %         
% %         imwrite(subImage,strcat(savePath,num2str(i),'.bmp'))
%     end
    
    [val,idx]=max([measurements.Area]);
    thisBoundingBox = measurements(idx).BoundingBox;
    subImage = imcrop(bw,thisBoundingBox);
    imwrite(subImage,strcat(savePath,'1.bmp'))
    
    %extract card image
    subImage = imcrop(bw,thisBoundingBox);
    %rotate image if needed
    [h,w]=size(subImage);
    if(h<w)
        subImage=imrotate(subImage,-90);
    end
    %resize a card image to 700*500
    reShape = imresize(subImage,[700,500]);
    
   
    %opening image to fill small holes
    SE = strel('square',3);
    bw2 = imopen(reShape,SE);
    %make a complement image
    bw3=imcomplement(bw2);
%    blob = regionprops('table',bw3,'MajorAxisLength','ConvexArea','FilledArea','Perimeter')
%     %----------------------------------------------------------------------
%     %use three features MajorAxisLength, ConvexArea, and BoundingBox to
%     %extract subImages.
%     %ConvexArea is used to separate left corner of rank and suit
%     %from major suit
%     %MajorAxisLength is used to distinguish suit and rank of left corner
%     %The axislength of rank is greater than suit
%     %BoundingBox is uesd to define the region of features
%     %----------------------------------------------------------------------
     blobMeasurements=regionprops(bw3,'MajorAxisLength','ConvexArea','BoundingBox','Perimeter');
%     %number of feature images
%     numberOfBlobs = size(blobMeasurements, 1);
% 
%     count_rank=0;
%     count_suit=0;
%     LargestAxisLength = 0;
%     rank_index = 0;
%     suit_index = 0;
%     %extract all contours of a card
    for k=1 : numberOfBlobs
%         %extract rank contour of the left corner
%         if blobMeasurements(k).ConvexArea > 950 && blobMeasurements(k).ConvexArea <3500 && count_rank<2
%             %figure out which one is the rank vis choosing maximun
%             %majorAxisLength
%             majorAxisLength = blobMeasurements(k).MajorAxisLength;
%             if(majorAxisLength>LargestAxisLength)
%                 LargestAxisLength=majorAxisLength;
%                 rank_index = k;
%             end
%             count_rank=count_rank+1;
%         end
%         
%         %extract major suit contour
%         if blobMeasurements(k).ConvexArea > 4000 ...
%                 && blobMeasurements(k).Perimeter > 150 ...
%                 && blobMeasurements(k).Perimeter <1000 ...
%                 && blobMeasurements(k).MajorAxisLength < 300 ...
%                 &&count_suit<1
%             suit_index = k;
%             count_suit = count_suit+1;
%         end
        thisBoundingBox = blobMeasurements(k).BoundingBox;
        saveFilePath = strcat(savePath,num2str(k),'.bmp');
        subImage = imcrop(bw3,thisBoundingBox);
        imwrite(subImage,saveFilePath);
    end
%     
%     %crop the rank image
%     thisBoundingBox=blobMeasurements(rank_index).BoundingBox;
%     rankImage = imcrop(bw3,thisBoundingBox);
%     saveFilePath = strcat(savePath,'rank.bmp');
%     imwrite(rankImage,saveFilePath);
%     
%     %crop the suit image
%     thisBoundingBox=blobMeasurements(suit_index).BoundingBox;
%     suitImage = imcrop(bw3,thisBoundingBox);
%     saveFilePath = strcat(savePath,'suit.bmp');
%     imwrite(suitImage,saveFilePath);
end