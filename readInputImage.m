clear
clc
readInputDirPath = 'c:\project\inputImage\';
inputFileLists = dir([readInputDirPath,'*.jpg']);
global card_num;
input_counts = length(inputFileLists);
card_num=1;
for i=1:input_counts
    image_name = inputFileLists(i).name;
    savedFolderPath = strcat(readInputDirPath,'card_',num2str(card_num));
    saveImagePath = strcat(savedFolderPath,'\');
    readImagePath = strcat(readInputDirPath,image_name);
    
    %save image folder
    createFolder(savedFolderPath);
    %read image file path
    processImage(readImagePath,saveImagePath);
    card_num=card_num+1;  
end 