%normalize suit image with rotate if needed
function resizeImageWithRotateIfNeeded(readPath,savePath)
    image = imread(readPath);
    r_image = imresize(image,[50 50]);
    blob = regionprops(r_image,'Centroid','ConvexArea');
    [area,idx] = max([blob.ConvexArea]);
    center=blob(idx).Centroid;
    %x > y means upsidedown
    if(center(1)>center(2))
        r_image = imrotate(r_image,180);
    end
    %saveFilePath = strcat(save_file_path,'.bmp');
    imwrite(r_image,savePath);
end