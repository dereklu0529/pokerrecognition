%this function is used to create target set
function [targetVector] =targetValue(suit)
    % 0 2 3 4 5 6 7 8 9 A c d h J K Q s
    persistent iden;
    iden=eye(17);
    switch suit
        case '0'
            targetVector=iden(1,:);
        case '2'
            targetVector=iden(2,:);
        case '3'
            targetVector=iden(3,:);
        case '4'
            targetVector=iden(4,:);
        case '5'
            targetVector=iden(5,:);
        case '6'
            targetVector=iden(6,:);
        case '7'
            targetVector=iden(7,:);
        case '8'
            targetVector=iden(8,:);
        case '9'
            targetVector=iden(9,:);
        case 'A'
            targetVector=iden(10,:);
        case 'c'
            targetVector=iden(11,:);
        case 'd'
            targetVector=iden(12,:);
        case 'h'
            targetVector=iden(13,:);
        case 'J'
            targetVector=iden(14,:);
        case 'K'
            targetVector=iden(15,:);
        case 'Q'
            targetVector=iden(16,:);
        case 's'
            targetVector=iden(17,:);
        otherwise
    end
end