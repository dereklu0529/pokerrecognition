clear;
clc;
readDirPath = 'c:\project\norm';
createTrainingData(readDirPath);

function createTrainingData(filePath)
    if exist('training_set.csv','file')==2
        delete('training_set.csv')
    end

    if exist('target_set.csv','file')==2
        delete('target_set.csv')
    end

    readPath =getPath(filePath);
    for k = 2 : length(readPath)
        readSubFileList = dir([readPath{k},'*.bmp']);
        for j =1 : length(readSubFileList)
            temp =strsplit(readPath{k}, '\');
            suit = temp{end-1};
            tarVector = targetValue(suit);
            image_path = strcat(readPath{k},readSubFileList(j).name);
            image = imread(image_path);
            image_T = transpose(image);
            imageVector = reshape(image_T,[1,2500]);
            dlmwrite('training_set.csv',imageVector,'delimiter',',','-append');
            dlmwrite('target_set.csv',tarVector,'delimiter',',','-append');
        end
    end
end

function [targetVector] =targetValue(suit)
    % 0 2 3 4 5 6 7 8 9 A c d h J K Q s
    persistent iden;
    iden=eye(17);
    switch suit
        case '0'
            targetVector=iden(1,:);
        case '2'
            targetVector=iden(2,:);
        case '3'
            targetVector=iden(3,:);
        case '4'
            targetVector=iden(4,:);
        case '5'
            targetVector=iden(5,:);
        case '6'
            targetVector=iden(6,:);
        case '7'
            targetVector=iden(7,:);
        case '8'
            targetVector=iden(8,:);
        case '9'
            targetVector=iden(9,:);
        case 'A'
            targetVector=iden(10,:);
        case 'c'
            targetVector=iden(11,:);
        case 'd'
            targetVector=iden(12,:);
        case 'h'
            targetVector=iden(13,:);
        case 'J'
            targetVector=iden(14,:);
        case 'K'
            targetVector=iden(15,:);
        case 'Q'
            targetVector=iden(16,:);
        case 's'
            targetVector=iden(17,:);
        otherwise
    end       
end