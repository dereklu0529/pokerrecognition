clear
clc
saveDirPath = '/Users/pluto/project/norm_n';
readDirPath = '/Users/pluto/project/save_n';

readPath =getPath(readDirPath);
savePath =getPath(saveDirPath);

%read images from save folders and resize images to specific folders
for k = 1 : length(readPath)
    readSubFileList = dir([readPath{k},'*.bmp']);
    
    cnt=0;
    for j =1 : length(readSubFileList)
        read_file_path = strcat(readPath{k},readSubFileList(j).name);
        save_file_path = strcat(savePath{k},num2str(cnt),'.bpm');
        resizeImage(read_file_path,save_file_path);
        cnt=cnt+1;
    end
end

%get process path
function [path] =getPath(file_path)
    gPath = genpath(file_path);
    length_gPath = size(gPath,2);
    path={};
    temp=[];
    index = 1;
    for i = 1 : length_gPath
        if gPath(i)~=':'
            temp = [temp gPath(i)];
        else
            temp = [temp '/'];
            path{index}=temp;
            index=index+1;
            temp=[];
        end
    end
    clear p length_p temp;
end

%resize Image
function resizeImage(read_file_path,save_file_path)
    image = imread(read_file_path);
    r_image = imresize(image,[50 50]);
    %blob = regionprops('table',r_image,'Centroid','Orientation')
    blob = regionprops(r_image,'Centroid','ConvexArea')
    [area,idx] = max([blob.ConvexArea])
    center=blob(idx).Centroid
% % % %   x > y mean upsidedown
    if(center(1)>center(2))
        r_image = imrotate(r_image,180);
    end
    saveFilePath = strcat(save_file_path,'.bmp');
    imwrite(r_image,saveFilePath);
end